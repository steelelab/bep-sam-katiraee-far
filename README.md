# BEP-Sam-Katiraee-Far

## Goals

What do we want to do? What is interesting to Sam? What is useful to us? What is feasible? 


Discussing with Jasper, we will propose we start with:

0. Reading about the basics
    * Piezoelectricity in general
    * Read about superconductivity
    * Look at qubits coupled to cavities (LC circuit), in general and in qutip
        * Sounds scary!
        * But if qubit = LC, then this is two LCs coupled to each other 
        * This: we did in assignment 2!
        * Maybe optional already: do the quantum version (QuCAT)

        
1. Build a conceptual understanding by studying and analysing an analytical model
    * Code and study the analytical model of a piezo capacitor from Amir's paper in a jupyter notebook
    * Plot and interpret the equivlent circuit model and use analytical formulas to predict the S11 kappa_ext and qubit couplings
    * Calculate the frequency dependent admittance from the model
    * Fit this with the [vector fit](https://github.com/PhilReinhold/vectfit_python) routine to extract the poles and convert this into a (foster decomposition) equivalent circuit
    * Program the coupling of this to a transmon qubit using QuCAT
    * Quantize it using QuCAT to get the cross-Kerr couplings 


2. Program a simple-as-possible "parallel plate capacitor" model of an Hbar into Comsol
    * Difference to the analytical model is that the piezo layer has a finite thickness
    * Start off picking up from Gary's comsol files 
    * Then pick a strategy for efficiently simulating the effect of the finite piezo thickness
    * We want to completely ignore radial modes: how? 
        * It's actually a 1D problem then. 
            * If we could sovle in 1D, then it would be mega computationally efficient. 
            * But I'm not sure if comsol can even do 1D? 
            * Maybe there is an analytical solution?
            * Or we could code it ourselves as a finite difference problem in python? :)
        * If 1D comsol / 1D analytical solution not possible:
            * Maybe a smart way to set up radially symmetric comsol to ignore radial modes
            * I kindof did this by making the thing mega narrow and really long. There were lots of "flopping" modes, but they did not couple to the impedance
            * Maybe there is a better way? 
    * Use frequency dependent impedance to then predict kappa_ext / qubit coupling for different thicknesses of piezo layer
        * Can we do better that our current design? 
        * What is optimal? 
        * By how much is the coupling reduced if the capacitor is not 100% filled with piezo? 
        * What if we put an electrode below the piezo as well, before the "substrate"? Does that make a huge difference? 
            * Would be technologically challenging but if there is a huge benefit, it could be worth it


3. Comsol design for a proper 3D hbar simulation
    * Hard, maybe beyond scope of project
    * Aims could be:
        * How much does acoustic loss out to the substrate limit Q? 
            * Does it decrease if we we make it thinner? 
            * How much can we gain if we try "trenches"? 
            * Or "hanging" on membrane? Or "tethers"? 
        * Planar electrode geometry:
            * How much do we lose in coupling if there is no metal on the bottom of the chip? 
            * Can we optimise the metal on the surface to get better coupling / higher Q? 
            



## Planning

## Other relevant GIT repositories

https://gitlab.tudelft.nl/steelelab/project-quantum-acoustics

A repository for the quantum acoustics project

https://gitlab.tudelft.nl/steelelab/piezo-equivalent-model

A repository that Gary has been using to document the stuff he's been playing around with and trying in Comsol

## Literature 

### Core references

https://journals.aps.org/pra/abstract/10.1103/PhysRevA.94.063864

A nice paper about modeling piezo-acoustic coupling from Amir's group, both classically and quantum

https://science.sciencemag.org/content/358/6360/199

The science paper from Yale, with "flat" electrodes

### More references

https://www.nature.com/articles/s41586-018-0717-7

The follow-up Nature paper where the used "curved" piezos to make an acoustic focussing lens

https://arxiv.org/abs/2006.05446

The recent work from Mika Sillanpaa's group flipping a sapphire chip on top of a qubit chip

https://arxiv.org/abs/1802.06642

Earlier Sillanpaa work with acoustics and qubit on one chip

